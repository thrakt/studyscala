//def upCase(s: String) = {
def upCase(s: String): String = {
  if (s.length == 0)
    return s
  else
    s.toUpperCase()
}

println( upCase("") )
println( upCase("Hello") )
