val dogBreeds = List("Doverman", "Yorkshire Terrier", "Dachshund",
                     "Scottish Terrier", "Great Dane", "Portuguese Water Dog")
for (breed <- dogBreeds
  if breed.contains("Terrier");
  if breed.startsWith("Yorkshire") == false
) println(breed)
