val bools = List(true, false)

for (bool <- bools) {
  bool match {
    case true => println("heads")
    case false => println("tails")
    case _ => println("something ogher tan heads or tails (yikes!)")
  }
}
