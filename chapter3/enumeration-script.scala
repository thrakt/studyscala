object Breed extends Enumeration {
  val doberman = Value("Doberman Pinscher")
  val yorkie = Value("Yorkshire Terrier")
  val scottie = Value("Scottish Terrier")
  val dane = Value("Great Dane")
  val portie = Value("Portuguese Water Dog")
}

println("ID\tBreed")

for (breed <- Breed.values) println(breed.id + "\t" + breed)

println("\nJust Terriers:")
Breed.values.filter(_.toString.endsWith("Terrier")).foreach(println)
