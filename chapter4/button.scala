package ui {
  class Button(val label: String) extends Widget{
    def click() = {
      // click action
    }
  }
}

package ui2 {
  import ui.Widget

  class Button(val label: String) extends Widget with Clickable {
    def click() = {
      // click action
    }
  }
}
